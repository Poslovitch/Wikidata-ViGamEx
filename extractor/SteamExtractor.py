import requests

from extractor.DataExtractor import DataExtractor


class SteamExtractor(DataExtractor):

    def query(self, app_id: int):
        req = requests.Request('GET', f"https://store.steampowered.com/api/appdetails?appids={app_id}")
        r = req.prepare()

        s = requests.Session()
        s.headers.update({'User-Agent': 'Poslovitch test', 'content-type': 'application/json'})
        result = s.send(r)
        print(result.reason)
        print(result.headers)
        print(result.text)
        # print(result.json())
